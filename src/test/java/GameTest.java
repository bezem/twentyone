import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {

    @Mock private Deck deck;
    @Mock private Hand sam;
    @Mock private Hand dealer;

    @Before
    public void setUp() throws Exception {
        when(deck.deal()).thenReturn(Card.CA);
    }

    @Test
    public void shouldDeclareSamWinnerWhenSamBlackjack() {
        when(sam.getValue()).thenReturn(21);
        Game game = new Game(deck, sam, dealer);

        Result result = game.play();

        assertEquals("sam", result.getWinner());
    }

    @Test
    public void shouldDeclareDealerWinnerWhenDealerBlackjack() {
        when(dealer.getValue()).thenReturn(21);
        Game game = new Game(deck, sam, dealer);

        Result result = game.play();

        assertEquals("dealer", result.getWinner());
    }

    @Test
    public void shouldDeclareDealerWinnerWhenBoth22() {
        when(sam.getValue()).thenReturn(22);
        when(dealer.getValue()).thenReturn(22);
        Game game = new Game(deck, sam, dealer);

        Result result = game.play();

        assertEquals("dealer", result.getWinner());
    }

    @Test
    public void shouldOnlyDealInitialCardsWhenSamOver17AndDealerAbove() {
        when(sam.getValue()).thenReturn(18);
        when(dealer.getValue()).thenReturn(19);
        Game game = new Game(deck, sam, dealer);

        Result result = game.play();

        verify(deck, times(4)).deal();
    }

    @Test
    public void shouldDealCardWhenSamUnder17() {
        when(sam.getValue())
                .thenReturn(14) // Blackjack check
                .thenReturn(14) // First draw check
                .thenReturn(18);
        when(dealer.getValue()).thenReturn(19);
        Game game = new Game(deck, sam, dealer);

        Result result = game.play();

        verify(deck, times(5)).deal();
        verify(sam, times(3)).addCard(any());
        verify(dealer, times(2)).addCard(any());
    }

    @Test
    public void shouldBustSamIfDrawGoesOver21() {
        when(sam.getValue())
                .thenReturn(14) // Blackjack check
                .thenReturn(14) // First draw check
                .thenReturn(22); // Bust
        when(dealer.getValue()).thenReturn(19);
        Game game = new Game(deck, sam, dealer);

        Result result = game.play();

        assertEquals("dealer", result.getWinner());
    }

    @Test
    public void shouldDealCardWhenDealerBelowSam() {
        when(sam.getValue()).thenReturn(19);
        when(dealer.getValue())
                .thenReturn(11) // Blackjack check
                .thenReturn(11) // First draw check
                .thenReturn(20);
        Game game = new Game(deck, sam, dealer);

        Result result = game.play();

        verify(deck, times(5)).deal();
        verify(sam, times(2)).addCard(any());
        verify(dealer, times(3)).addCard(any());
    }

    @Test
    public void shouldBustDealerIfDrawGoesOver21() {
        when(sam.getValue()).thenReturn(19);
        when(dealer.getValue())
                .thenReturn(13) // Blackjack check
                .thenReturn(13) // First draw check
                .thenReturn(24); // Bust
        Game game = new Game(deck, sam, dealer);

        Result result = game.play();

        assertEquals("sam", result.getWinner());
    }
}
