import org.junit.Test;

import static org.junit.Assert.*;

public class DeckTest {

    @Test
    public void shouldInitializeFullDeck() {
        Deck fullDeck = new Deck();

        assertEquals(52, fullDeck.size());
    }

    @Test
    public void shouldCreateDeckOfCorrectSize() {
        Deck deck = new Deck("S10, DA, H6");

        assertEquals(3, deck.size());
    }

    @Test
    public void shouldHandleWhitespace() {
        Deck deck = new Deck("\t  S10,      \t              DA        ");

        assertEquals(2, deck.size());
    }

    @Test
    public void shouldReturnAndRemoveFirstElement() {
        Deck deck = new Deck("S10, DA, H6");

        assertEquals(Card.S10, deck.deal());
        assertEquals(2, deck.size());
    }

    @Test
    public void shouldParseValidFile() {
        Deck deck = Deck.fromFile("src/test/resources/sample.txt");

        assertEquals(5, deck.size());
    }
}
