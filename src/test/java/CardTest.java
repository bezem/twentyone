import org.junit.Test;

import static org.junit.Assert.*;

public class CardTest {

    @Test
    public void shouldParseStringsCorrectly() {
        assertEquals(Card.C10, Card.valueOf("C10"));
        assertEquals(Card.DA, Card.valueOf("DA"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowErrorWhenStringDoesNotMatchAny() {
        Card.valueOf("AA");
    }

    @Test
    public void shouldReturnScore() {
        assertEquals(11, Card.HA.getScore());
        assertEquals(3, Card.S3.getScore());
    }
}
