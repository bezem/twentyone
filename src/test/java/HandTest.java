import org.junit.Test;

import static org.junit.Assert.*;

public class HandTest {

    @Test
    public void shouldStartAtZeroValue() {
        Hand hand = new Hand();
        assertEquals(0, hand.getValue());
    }

    @Test
    public void shouldIncreaseByCardScore() {
        Hand hand = new Hand();
        hand.addCard(Card.C2);
        assertEquals(2, hand.getValue());

        hand.addCard(Card.DA);
        assertEquals(13, hand.getValue());
    }

    @Test
    public void shouldOutputAsSingleLine() {
        Hand hand = new Hand();
        hand.addCard(Card.C2);
        hand.addCard(Card.DA);

        assertEquals("C2, DA", hand.toString());
    }
}
