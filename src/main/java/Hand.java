import java.util.LinkedList;
import java.util.List;

public class Hand {

    private List<Card> cards = new LinkedList<>();

    public int getValue() {
        int sum = 0;
        for (Card card: this.cards) {
            sum += card.getScore();
        }
        return sum;
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Card card : cards) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(card);
        }
        return sb.toString();
    }
}
