public class Result {
    private String winner;
    private Hand sam;
    private Hand dealer;

    public Result(String winner, Hand sam, Hand dealer) {
        this.winner = winner;
        this.sam = sam;
        this.dealer = dealer;
    }

    public void print() {
        System.out.println(winner);
        System.out.println("sam: " + sam);
        System.out.println("dealer: " + dealer);
    }

    public String getWinner() {
        return winner;
    }
}
