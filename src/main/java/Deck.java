import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

public class Deck {

    private LinkedList<Card> cards = new LinkedList<>();

    public Deck() {
        this.cards.addAll(Arrays.asList(Card.values()));
        Collections.shuffle(this.cards);
    }

    public Deck(String deck) {
        for (String cardAsString : deck.split(",")) {
            this.cards.add(Card.valueOf(cardAsString.trim()));
        }
    }

    public int size() {
        return cards.size();
    }

    public Card deal() {
        return cards.remove(0);
    }

    public static Deck fromFile(String filename) {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            return new Deck(br.readLine());
        } catch (IOException e) {
            throw new IllegalArgumentException("Error reading provided file", e);
        }
    }
}
