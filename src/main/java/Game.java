public class Game {

    private Deck deck;
    private Hand sam;
    private Hand dealer;

    public Game(Deck deck, Hand sam, Hand dealer) {
        this.deck = deck;
        this.sam = sam;
        this.dealer = dealer;
    }

    public Result play() {
        sam.addCard(deck.deal());
        dealer.addCard(deck.deal());
        sam.addCard(deck.deal());
        dealer.addCard(deck.deal());

        if (sam.getValue() == 21) {
            return new Result("sam", sam, dealer);
        }
        if (dealer.getValue() == 21) {
            return new Result("dealer", sam, dealer);
        }
        while (sam.getValue() <= 17) {
            sam.addCard(deck.deal());
        }
        if (sam.getValue() > 21) {
            return new Result("dealer", sam, dealer);
        }
        while (dealer.getValue() <= sam.getValue()) {
            dealer.addCard(deck.deal());
        }
        if (dealer.getValue() > 21) {
            return new Result("sam", sam, dealer);
        }
        return new Result("dealer", sam, dealer);
    }

    public static void main(String[] args) {
        Game game;
        if (args.length > 0) {
             game = new Game(Deck.fromFile(args[0]), new Hand(), new Hand());
        } else {
             game = new Game(new Deck(), new Hand(), new Hand());
        }
        game.play().print();
    }
}
