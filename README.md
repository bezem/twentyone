### Twentyone

This project implements a variant of the game Twentyone. It uses a maven for building a runnable jar. 

#### Build

To build the jar of this project the following command is used:

`mvn clean install`

This will also run the unit-tests

#### Run

When built this project can be run independently of maven. It accepts an optional argument of a path to a file describing a card deck. Run the following in a terminal:

`java -jar target/twentyone-0.1.0-SNAPSHOT.jar src/test/resources/sample.txt`


